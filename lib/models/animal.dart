// Animals used in some games
enum Animal { cat, duck, dog }

extension AnimalInfo on Animal {
  // Animal name
  String get name => toString().split('.').last;

  // Animal sound filename
  String get fileName => '${name}_1.wav';

  // Animal emoji
  String get emoji {
    if (this == Animal.cat) return '🐈';
    if (this == Animal.dog) return '🐕';
    if (this == Animal.duck) return '🦆';
    return 'Invalid';
  }
}
