import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'game_category.i18n.dart';

enum GameCategory { Logic, Sound, Vibration }

extension CategoryIcon on GameCategory {
  // Category name
  String get name => describeEnum(this).i18n;

  // Category icon
  IconData get icon {
    if (this == GameCategory.Sound) return Icons.volume_up;
    if (this == GameCategory.Vibration) return Icons.vibration;
    if (this == GameCategory.Logic) return Icons.category;
    return Icons.error;
  }
}
