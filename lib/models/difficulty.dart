import 'package:flutter/foundation.dart';

import 'difficulty.i18n.dart';

// Game difficulty
enum Difficulty { Easiest, Easy, Medium, Hard, Hardest }

extension DifficultyName on Difficulty {
  String get name => describeEnum(this).i18n;
}
