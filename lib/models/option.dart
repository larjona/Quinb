import 'package:flutter/material.dart';
import 'option.i18n.dart';

// Game option
class Option {
  // Option value (used as ID)
  int value;

  // Text displayed
  String _text;
  String get text => _text ?? value.toString();

  // Option color
  Color color;

  Option(this.value, {String text, this.color}) {
    _text = text;
  }

  // Single option
  static List<Option> get single => [Option.tap];
  static Option get tap => Option(-999, text: 'Tap'.i18n);
}

// Convert list of int into list of Option
extension ToOptions on List<int> {
  List<Option> toOptions({bool asColors = false}) {
    var output = <Option>[];
    forEach((value) {
      output.add(Option(value,
          color: asColors ? Color(value) : null, text: asColors ? ' ' : null));
    });
    return output;
  }
}
