import 'package:flutter/material.dart';

import 'package:quinb/util/color_name.dart';
import 'player.i18n.dart';

// Class used to store player details
class Player {
  // Player id. It equals to his index in game.playerList
  final int id;

  // Player color
  final Color color;

  // Player name
  String get name => 'Player'.i18n + ' ${color.name}';

  // Player points
  int points = 0;

  Player({this.id, this.color});
}
