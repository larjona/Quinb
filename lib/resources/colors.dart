import 'package:flutter/material.dart';

// Colors used in some game
final List<Color> colorList = [
  Colors.blue,
  Colors.red,
  Colors.green,
  Colors.purple,
  Colors.orange,
  Colors.black,
  Colors.brown,
];

// Colors of the players
List<Color> playerColor = [
  Colors.blueAccent,
  Colors.redAccent,
  Colors.green,
  Colors.purple,
];
