// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'ru': {
          'Reflexes': 'Рефлексы',
          'Remember': 'Запомни',
          'How many times have you heard the vibration?':
              'Сколько раз ты услышал вибрацию?',
          'Three figures': 'Три фигуры',
          'Find the shortest vibration': 'Найди короткую вибрацию',
          'As soon as the screen becomes colored, tap the option of the same color.\n\nKeep in mind that you have little time before the screen turns white again.':
              'Как только экран будет покрашен, нажмите на выбранный цвет.\n\nПомни, что у тебя мало времени до того момента как экран снова станет белым.',
          'Remember the colors and letters that appear':
              'Запомни цвета и надписи',
          'Tap when there are 3 identical figures':
              'Нажми когда будет 3 идентичные фигуры',
          'Mix the colors': 'Смешайте цвета',
          'Count how many lights come on':
              'Посчитай сколько огней будет включенно',
          'Tap at the right moment': 'Нажмите в нужный момент',
          'Count how many times you hear the vibration':
              'Посчитай сколько раз ты услышишь вибрацию',
          'How many times have you heard the sound?':
              'Сколько раз ты услышал звук?',
          'Tap the screen when you see at least 3 figures that have the same color and the same letter inside':
              'Нажми на экран когда ты увидишь ',
          'Shortest vibration': 'Короткая вибрация',
          'Fastest object': 'Быстрый объект',
          'Mix Colors': 'Смесь Цветов',
          'How many light have come on?': 'Сколько огней было включенно?',
          'Which one was the shortest vibration?':
              'Какая вибрация была короткой?',
          'Find three identical figures': 'Найди три идентичные фигуры',
          'Find the fastest object': 'Найди быстрый объект',
          'Count the lights': 'Посчитай цвета',
          'Count how many times you hear the sound':
              'Посчитай сколько раз ты услышишь звук',
        },
        'it': {
          'Remember': 'Ricorda',
        }
      };

  String get i18n => localize(this, _t);
}
