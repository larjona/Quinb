import 'package:i18n_extension/i18n_widget.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:quinb/ui/screens/home/home_page.dart';
import 'package:quinb/ui/screens/rules/rules_page.dart';
import 'package:quinb/ui/theme.dart';
import 'package:quinb/util/local_data_controller.dart';
import 'package:quinb/util/volume_controller.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await loadStoredData();
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  await initVolumeController();
  runApp(Quinb());
}

class Quinb extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return I18n(
      initialLocale: settings.locale == null
          ? null
          : Locale(settings.locale[0], settings.locale[1]),
      child: MaterialApp(
        title: 'Quinb',
        theme: theme,
        home: settings.firstRun ? RulesPage(HomePage()) : HomePage(),
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('de', 'DE'),
          const Locale('en', 'UK'),
          const Locale('es', 'ES'),
          const Locale('fr', 'FR'),
          const Locale('it', 'IT'),
          const Locale('ru', 'RU'),
        ],
      ),
    );
  }
}
