import 'package:flutter/material.dart';
import 'basic.i18n.dart';

// Linear gradient used in app
const appGradient = LinearGradient(colors: [
  Color(0xEE008080),
  Color(0xEE106075),
  Color(0xEE155070),
  Color(0xEE204569),
  Color(0xEE254069),
  Color(0xEE353769),
  Color(0xEE393469),
], begin: Alignment.topLeft, end: Alignment.bottomRight);

// Page route
class FadeRoute extends PageRouteBuilder {
  @override
  final Duration transitionDuration = const Duration(milliseconds: 150);

  final Widget page;
  FadeRoute(this.page)
      : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page,
          transitionsBuilder: (BuildContext context,
                  Animation<double> animation,
                  Animation<double> secondaryAnimation,
                  Widget child) =>
              FadeTransition(opacity: animation, child: child),
        );
}

// SnackBar shown if volume == 0 or if sound-based games are disabled/enabled
void volumeSnackBar(GlobalKey<ScaffoldState> key,
    {bool floating = false, String text}) {
  text ??= 'Increase system volume in order to play all the games'.i18n;
  key.currentState.removeCurrentSnackBar();
  key.currentState.showSnackBar(SnackBar(
    content: Text(
      text,
      style: const TextStyle(fontSize: 16),
      textAlign: TextAlign.center,
    ),
    duration: const Duration(seconds: 2),
    behavior: floating ? SnackBarBehavior.floating : SnackBarBehavior.fixed,
    elevation: 3,
    backgroundColor: floating ? const Color(0xCC155070) : null,
    shape: floating
        ? RoundedRectangleBorder(borderRadius: BorderRadius.circular(30))
        : null,
  ));
}
