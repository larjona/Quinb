import 'package:flutter/material.dart';

// App theme
final theme = ThemeData(
  primarySwatch: Colors.teal,
  primaryColor: const Color(0xFF155070),
  accentColor: const Color(0xFF106075),
  canvasColor: Colors.white,
  appBarTheme: AppBarTheme(
    textTheme: TextTheme(
      headline6: TextStyle(
        letterSpacing: 6,
        fontSize: 22,
        fontWeight: FontWeight.w600,
      ),
    ),
  ),
);
