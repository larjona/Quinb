// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'ru': {
          'Goal': 'Цель',
          'Points': 'Очки',
          'That\'s all': 'Это всё',
          'Multiplayer': 'Сетевая игра',
          'Games': 'Игры',
        },
        'it': {
          'Points': 'Punti',
          'Games': 'Giochi',
        }
      };

  String get i18n => localize(this, _t);
}
