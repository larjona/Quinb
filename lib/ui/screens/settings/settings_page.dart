import 'package:flutter/material.dart';

import 'package:quinb/models/difficulty.dart';
import 'package:quinb/ui/screens/settings/widgets/restore_dialog.dart';
import 'package:quinb/util/local_data_controller.dart';
import 'settings_page.i18n.dart';

class SettingsPage extends StatefulWidget {
  // Option values
  final maxTurnsValues = <int>[18, 21, 24, 27, 30, 99];
  final timeAvailableValues = <int>[3, 5, 7, 9, 11];
  final requiredPointsValues = <int>[3, 5, 7, 9, 11];

  @override
  SettingsPageState createState() => SettingsPageState();
}

class SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('SETTINGS'.i18n),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.settings_backup_restore),
            tooltip: 'Restore'.i18n,
            onPressed: () {
              restoreSettingsDialog(this);
            },
          ),
        ],
      ),
      body: ListView(
        children: <Widget>[
          CheckboxListTile(
            title: Text('Pause between rounds'.i18n,
                style: const TextStyle(fontSize: 20)),
            subtitle: Text('Wait before starting the next round'.i18n),
            value: settings.startPaused,
            onChanged: (newValue) {
              setState(() {
                settings.startPaused = newValue;
                saveSettings();
              });
            },
          ),
          ListTile(
            title:
                Text('Difficulty'.i18n, style: const TextStyle(fontSize: 20)),
            subtitle: Text('Difficulty of the games'.i18n),
            trailing: DropdownButton<Difficulty>(
              value: settings.difficulty,
              onChanged: (Difficulty newValue) {
                setState(() {
                  settings.difficulty = newValue;
                  saveSettings();
                });
              },
              items: Difficulty.values
                  .map<DropdownMenuItem<Difficulty>>(
                    (Difficulty value) => DropdownMenuItem<Difficulty>(
                      value: value,
                      child: Text(value.name),
                    ),
                  )
                  .toList(),
            ),
          ),
          ListTile(
            title: Text('Points required'.i18n,
                style: const TextStyle(fontSize: 20)),
            subtitle: Text('Points required to win'.i18n),
            trailing: DropdownButton<int>(
              value: settings.requiredPoints,
              onChanged: (int newValue) {
                setState(() {
                  settings.requiredPoints = newValue;
                  saveSettings();
                });
              },
              items: widget.requiredPointsValues
                  .map<DropdownMenuItem<int>>(
                    (int value) => DropdownMenuItem<int>(
                      value: value,
                      child: Text(
                        value.toString().padRight(3, ' '),
                      ),
                    ),
                  )
                  .toList(),
            ),
          ),
          ListTile(
            title: Text('Round duration'.i18n,
                style: const TextStyle(fontSize: 20)),
            subtitle: Text('Time available each round (in seconds)'.i18n),
            trailing: DropdownButton<int>(
              value: settings.timeAvailable,
              onChanged: (int newValue) {
                setState(() {
                  settings.timeAvailable = newValue;
                  saveSettings();
                });
              },
              items: widget.timeAvailableValues
                  .map<DropdownMenuItem<int>>(
                    (int value) => DropdownMenuItem<int>(
                      value: value,
                      child: Text(
                        value.toString().padRight(3, ' '),
                      ),
                    ),
                  )
                  .toList(),
            ),
          ),
          ListTile(
            title: Text('Rounds'.i18n, style: const TextStyle(fontSize: 20)),
            subtitle: Text('Maximum number of rounds'.i18n),
            trailing: DropdownButton<int>(
              value: settings.maxRounds,
              onChanged: (int newValue) {
                setState(() {
                  settings.maxRounds = newValue;
                  saveSettings();
                });
              },
              items: widget.maxTurnsValues
                  .map<DropdownMenuItem<int>>(
                    (int value) => DropdownMenuItem<int>(
                      value: value,
                      child: Text(
                        value.toString().padRight(3, ' '),
                      ),
                    ),
                  )
                  .toList(),
            ),
          ),
        ],
      ),
    );
  }

  void refresh() {
    setState(() {});
  }
}
