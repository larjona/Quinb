// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'ru': {
          'Points required': 'Очков требуется',
          'Round duration': 'Продолжительность раунда',
          'Points required to win': 'Очков требуется для победы',
          'Maximum number of rounds': 'Максимальное число раундов',
          'Time available each round (in seconds)':
              'Время доступное на раунд (в секундах)',
          'SETTINGS': 'НАСТРОЙКИ',
          'Restore': 'Восстановить',
          'Wait before starting the next round':
              'Подождите пока начнётся следующий раунд',
          'Rounds': 'Раунды',
          'Pause between rounds': 'Пауза между раундами',
          'Difficulty': 'Сложность',
          'Difficulty of the games': 'Сложность игр',
        },
        'it': {
          'SETTINGS': 'IMPOSTAZIONI',
        }
      };

  String get i18n => localize(this, _t);
}
