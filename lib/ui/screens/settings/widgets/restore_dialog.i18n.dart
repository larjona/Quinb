// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'it': {
          'Restore': 'Ripristina',
          'Restore default settings?': 'Ripristinare impostazioni predefinite?',
        }
      };

  String get i18n => localize(this, _t);
}
