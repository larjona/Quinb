// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'ru': {
          'Cancel': 'Отмена',
          'Rules': 'Правила',
          'Remember choice': 'Запомните выбор',
          'Sound-based games disabled': 'Звуковые игры выключены',
          'player': 'игрок',
          'Play': 'Играть',
          'Sound games': 'Звуки игры',
          'Select game': 'Выбери ',
          'Enable volume?': 'Включить громкость?',
          'players': 'игроки',
          '\nIt looks like you have audio muted.\n\nYou should enable it in order to play sound-based games.\n':
              '\nПохоже на то, что аудио приглушенно.\n\nТебе нужно включить его, чтобы играть в звуковые игры.\n',
          'Enable': 'Включить',
          'Sound-based games enabled': 'Звуковые игры включены',
          'Settings': 'Настройки',
        },
        'it': {
          'Play': 'Gioca',
          'Rules': 'Regole',
          'Select game': 'Seleziona gioco',
          'Settings': 'Impostazioni',
        }
      };

  String get i18n => localize(this, _t);
}
