// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'ru': {
          'Settings': 'Настройки',
          'Rules': 'Правила',
          'players': 'игроки',
          'GAMES': 'ИГРЫ',
          'Sound games': 'Звуки игр',
          'All': 'Всё',
          'Sound-based games enabled': 'Звуковые игры включены',
          'Sound-based games disabled': 'Звуковые игры выключены',
          'Info': 'Информация',
          'player': 'игрок',
        },
        'it': {
          'GAMES': 'GIOCHI',
        }
      };

  String get i18n => localize(this, _t);
}
