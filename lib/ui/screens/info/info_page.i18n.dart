// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'ru': {
          'Look at the source code': 'Посмотри на исходный код',
          'Report bugs or request new feature':
              'Сообщить об ошибках или запросить новую функцию',
          'By': 'От',
          'View app changelog': 'Посмотреть список изменений приложения',
          'Read software license': 'Прочитать лицензия приложения',
          'Version:': 'Версия:',
          'Changelog': 'Список изменений',
          'View source code': 'Посмотреть исходный код',
          'Read third party notices': 'Читать сторонние лицензии',
          'Report bugs': 'Сообщить об ошибках',
          'App developed by': 'Приложение разработано',
          'View License': 'Посмотреть лицензию',
          'App version': 'Версия приложения',
          'Third Party Licenses': 'Сторонние лицензии',
        },
        'it': {
          'By': 'Da',
          'App developed by': 'App sviluppata da',
        }
      };

  String get i18n => localize(this, _t);
}
