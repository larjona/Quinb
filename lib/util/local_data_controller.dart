import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:quinb/models/settings.dart';

// Game settings
Settings settings;

SharedPreferences _prefs;

// Import data from shared preferences
Future<void> loadStoredData() async {
  _prefs = await SharedPreferences.getInstance();
  _loadSettings();
}

// Save app settings in shared preferences
void saveSettings() {
  _prefs.setString('settings', jsonEncode(settings.toJson()));
}

// Restore app settings
void _loadSettings() {
  settings = Settings(jsonDecode(_prefs.getString('settings') ?? '{}'));
}
