// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'ru': {
          'Invalid': 'Неправильный',
          'Yellow': 'Жёлтый',
          'Red': 'Красный',
          'Black': 'Чёрный',
          'Green': 'Зелёный',
          'Blue': 'Синий',
          'Purple': 'Фиолетовый',
          'Brown': 'Коричневый',
        },
        'it': {
          'Red': 'Rosso',
          'Blue': 'Blu',
        }
      };

  String get i18n => localize(this, _t);
}
