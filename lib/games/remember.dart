import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';

import 'package:quinb/games/abstract_basic.dart';
import 'package:quinb/models/option.dart';
import 'package:quinb/resources/colors.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/util/color_name.dart';
import 'package:quinb/util/local_data_controller.dart';
import 'remember.i18n.dart';

// Remember the sequence of figures
class RememberGame extends BasicGame {
  RememberGame(int players, GamePageState gamePage) : super(players, gamePage);

  @override
  String get tip =>
      (element == Element.color
          ? 'What color was the '.i18n
          : 'What letter was in the'.i18n) +
      '${ordinal[correctValue]} ' +
      'figure?'.i18n;

  // Ordinal numbers
  final ordinal = <String>[
    '1st'.i18n,
    '2nd'.i18n,
    '3rd'.i18n,
    '4th'.i18n,
    '5th'.i18n,
    '6th'.i18n
  ];

  // Letters
  String currentLetter;
  List<String> letterSequence = ['A', 'B', 'C', 'D', 'E', 'F'];

  // Colors
  Color currentColor;
  List<Color> colorSequence;

  // Letter or color
  Element element;

  @override
  void init() {
    letterSequence = letterSequence.sublist(0, settings.difficulty.index + 2);
    colorSequence = colorList.sublist(0, settings.difficulty.index + 2);
    super.init();
  }

  @override
  void prePhase() async {
    element = Element.values[Random().nextInt(Element.values.length)];
    colorSequence.shuffle();
    letterSequence.shuffle();
    var tempList =
        List.from(element == Element.color ? colorSequence : letterSequence);
    optionsAvailable = [
      for (var t in tempList)
        Option(tempList.indexOf(t),
            text: element == Element.color ? (t as Color).name : t)
    ]..shuffle();
    correctValue =
        optionsAvailable[Random().nextInt(optionsAvailable.length)].value;

    for (var i = 0; i < letterSequence.length; i++) {
      if (!isPrePhase) return;
      currentColor = colorSequence[i];
      currentLetter = letterSequence[i];
      gamePage.refresh();
      await Future.delayed(const Duration(seconds: 1));
    }
  }

  @override
  Widget widget() => isPrePhase
      ? Container(
          height: 150,
          child: Center(
            child: Container(
              height: 100,
              width: 100,
              color: currentColor,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  if (playerList.length > 1)
                    RotatedBox(quarterTurns: 2, child: _text()),
                  _text()
                ],
              ),
            ),
          ),
        )
      : super.widget();

  Widget _text() => Text(currentLetter,
      style: const TextStyle(fontSize: 40, color: Colors.white));
}

enum Element { letter, color /*, shape*/ }
