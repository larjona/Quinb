// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'ru': {
          'You won!': 'Ты победил!',
          '\nYou lost!': '\nТы проиграл!',
          'Too many mistakes. ': 'Слишком много попыток. ',
          'P A U S E': 'П А У З А',
          'won!': 'победил!',
          'Nobody won!': 'Никто не победил!',
        },
        'it': {
          'Too many rounds': 'Troppi round',
        }
      };

  String get i18n => localize(this, _t);
}
