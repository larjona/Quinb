// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'ru': {
          'Tap when you hear a': 'Нажми когда ты услышишь',
        },
        'it': {
          'Tap when you hear a': 'Tocca quando senti un',
        }
      };

  String get i18n => localize(this, _t);
}
