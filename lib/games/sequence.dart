import 'dart:async';
import 'dart:math';
import 'package:flutter/foundation.dart';
import 'package:vibration/vibration.dart';

import 'package:quinb/games/abstract_basic.dart';
import 'package:quinb/games/audio_mixin.dart';
import 'package:quinb/models/animal.dart';
import 'package:quinb/models/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/util/local_data_controller.dart';

// Sequence game
class SequenceGame extends BasicGame with AudioGame {
  // Vibration or Sound
  bool useVibration;

  SequenceGame(int players, GamePageState gamePage, {this.useVibration = false})
      : super(players, gamePage);

  // Represent the sequence
  String sequenceToString(List<dynamic> sequence) => sequence.fold(
      '',
      (previousValue, element) =>
          previousValue +
          (useVibration ? (element ? '__' : '.') : (element as Animal).emoji) +
          ' ');

  @override
  void prePhase() async {
    optionsAvailable.clear();
    var sequenceLength = 2 + settings.difficulty.index + Random().nextInt(3);
    var sequences = <List<dynamic>>[];
    var i = 0;
    while (sequences.length < min(5, 2 + settings.difficulty.index) && i < 42) {
      var _sequence = List.generate(
          sequenceLength,
          (_) => useVibration
              ? Random().nextBool()
              : Animal.values[Random().nextInt(Animal.values.length)]);
      if (sequences.every((element) => !listEquals(element, _sequence))) {
        sequences.add(_sequence);
        optionsAvailable.add(
            Option(optionsAvailable.length, text: sequenceToString(_sequence)));
      }
      i++;
    }
    correctValue = Random().nextInt(optionsAvailable.length);
    for (var element in sequences[correctValue]) {
      if (!isPrePhase) return;
      await (useVibration
          ? Vibration.vibrate(duration: element ? 300 : 150)
          : audioCache.play((element as Animal).fileName));
      await Future.delayed(const Duration(seconds: 1));
    }
  }
}
