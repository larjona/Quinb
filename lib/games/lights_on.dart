import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';

import 'package:quinb/games/abstract_basic.dart';
import 'package:quinb/models/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/util/local_data_controller.dart';

// Lights on game
class LightsOnGame extends BasicGame {
  LightsOnGame(int players, GamePageState gamePage) : super(players, gamePage);

  // Total number of lights
  final int lights = 16 + settings.difficulty.index * 4;

  // Time the lights stay on
  final int _time = 500 + (6 - settings.difficulty.index) * 100;

  // Lights off
  List<int> lightsOff = [];

  // Lights on now
  List<bool> currentLights = [];

  @override
  void prePhase() async {
    await Future.delayed(const Duration(seconds: 2));
  }

  @override
  void mainPhase() async {
    _switchOnLights();
    updater = Timer.periodic(Duration(milliseconds: _time), (_) {
      if (!isMainPhase || isPostPhase) return;
      _switchOnLights();
      gamePage.refresh();
    });
  }

  // Switch on random lights
  void _switchOnLights() {
    currentLights = List.generate(lights, (_) => Random().nextBool());
    correctValue = currentLights.where((l) => l).length > lights / 2
        ? Option.tap.value
        : 0;
  }

  @override
  Widget widget() => isMainPhase || isPostPhase
      ? Wrap(
          children: [
            InkWell(
              onTap: isMainPhase ? pause : null,
              child: GridView.count(
                physics: const ClampingScrollPhysics(),
                shrinkWrap: true,
                crossAxisCount: 4 + settings.difficulty.index,
                children: List.generate(
                  lights,
                  (index) => Container(
                    decoration: BoxDecoration(
                      color: currentLights[index]
                          ? Colors.orange
                          : Colors.grey[800],
                      border: Border.all(color: Colors.black),
                    ),
                  ),
                ),
              ),
            ),
          ],
        )
      : super.widget();
}
