// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'ru': {
          '6th': '6ой',
          'What letter was in the': 'Что написано в',
          '1st': '1ый',
          '2nd': '2ой',
          '3rd': '3ий',
          'figure?': 'фигуре?',
          '4th': '4ый',
          'What color was the ': 'Какого цвета ',
          '5th': '5ый',
        },
        'it': {
          'What color was the ': 'Di che colore era il ',
        }
      };

  String get i18n => localize(this, _t);
}
