import 'dart:async';
import 'dart:math';
import 'package:flutter/material.dart';
import 'package:vibration/vibration.dart';

import 'package:quinb/games/abstract_basic.dart';
import 'package:quinb/games/audio_mixin.dart';
import 'package:quinb/models/animal.dart';
import 'package:quinb/models/option.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/util/local_data_controller.dart';

// Listen and calculate
class ListenMathGame extends BasicGame with AudioGame {
  // True if outputs are vibrations instead of sounds
  bool useVibration;

  ListenMathGame(int players, GamePageState gamePage,
      {this.useVibration = false})
      : super(players, gamePage);

  // Sequence of sounds/vibrations
  List<bool> sequence = [];

  // Sum and Subtraction amount
  // Sum: duck / long vibration
  // Sub: cat / short vibration
  int sumValue;
  int subValue;

  @override
  void prePhase() async {
    sequence = List.generate(
        4 + Random().nextInt(2 + settings.difficulty.index),
        (_) => Random().nextBool());
    sumValue = max(1, settings.difficulty.index + Random().nextInt(4));
    subValue = min(-1, -settings.difficulty.index - Random().nextInt(4));
    correctValue = sequence.fold(
        0,
        (previousValue, element) =>
            previousValue + (element ? sumValue : subValue));
    var startIndex = correctValue - 4 + Random().nextInt(4);
    optionsAvailable =
        List<int>.generate(4, (i) => startIndex + i + 1).toOptions();
    for (var isSum in sequence) {
      if (!isPrePhase) return;
      await (useVibration
          ? Vibration.vibrate(duration: isSum ? 300 : 150)
          : audioCache
              .play(isSum ? Animal.duck.fileName : Animal.cat.fileName));
      await Future.delayed(const Duration(seconds: 1));
    }
  }

  @override
  Widget widget() => isPrePhase
      ? Container(
          height: playerList.length > 1 ? 250 : 125,
          margin: const EdgeInsets.symmetric(vertical: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              if (playerList.length > 1)
                RotatedBox(quarterTurns: 2, child: _widget()),
              _widget(),
            ],
          ),
        )
      : super.widget();

  // Inner widget
  Widget _widget() => Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(useVibration ? '__' : Animal.duck.emoji,
                  style: const TextStyle(fontSize: 40)),
              Text('+$sumValue', style: const TextStyle(fontSize: 40)),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(useVibration ? '- ' : Animal.cat.emoji,
                  style: const TextStyle(fontSize: 40)),
              Text('$subValue', style: const TextStyle(fontSize: 40)),
            ],
          ),
        ],
      );
}
