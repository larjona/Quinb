// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'ru': {
          'Third': 'Третий',
          'Fourth': 'Четвёртый',
          'Second': 'Второй',
          'First': 'Первый',
        },
        'it': {
          'Third': 'Terzo',
          'Fourth': 'Quarto',
          'Second': 'Secondo',
          'First': 'Primo',
        }
      };

  String get i18n => localize(this, _t);
}
