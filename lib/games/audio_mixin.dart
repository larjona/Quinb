import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';

import 'package:quinb/games/abstract_basic.dart';
import 'package:quinb/resources/sound_assets.dart';

// Add audio capabilities to BasicGame
mixin AudioGame on BasicGame {
  // AudioPlayer. Can play one audio at a time.
  final _audioPlayer = AudioPlayer(mode: PlayerMode.LOW_LATENCY)
    ..setReleaseMode(ReleaseMode.STOP);

  // Audio cache instance that represents a cache for Local Assets to be played
  AudioCache audioCache;

  @override
  void init() async {
    audioCache = AudioCache(prefix: 'audio/', fixedPlayer: _audioPlayer);
    await audioCache.loadAll(soundList);
    super.init();
  }
}
