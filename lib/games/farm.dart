import 'dart:async';
import 'dart:math';

import 'package:quinb/games/abstract_basic.dart';
import 'package:quinb/games/audio_mixin.dart';
import 'package:quinb/models/animal.dart';
import 'package:quinb/resources/sound_assets.dart';
import 'package:quinb/ui/screens/game/game_page.dart';
import 'package:quinb/util/local_data_controller.dart';
import 'farm.i18n.dart';

// Animal farm game
class FarmGame extends BasicGame with AudioGame {
  FarmGame(int players, GamePageState gamePage) : super(players, gamePage);

  @override
  String get tip => 'Tap when you hear a'.i18n + ' ${correctAnimal.name}';

  // Sequence of sounds
  List<String> sequence;

  // Animal to find
  Animal correctAnimal;

  // Index in sequence of the sound being played
  int currentlyPlayingIndex;

  @override
  void prePhase() async {
    correctAnimal = Animal.values[Random().nextInt(Animal.values.length)];
    sequence = List.from(soundList)
      ..retainWhere((s) => s.split('_').first == 'human');
    sequence.shuffle();
    for (var animal
        in Animal.values.where((element) => element != correctAnimal)) {
      sequence[Random().nextInt(settings.timeAvailable)] = animal.fileName;
    }
    sequence[Random().nextInt(settings.timeAvailable)] = correctAnimal.fileName;
    currentlyPlayingIndex = -1;
    await Future.delayed(const Duration(seconds: 2));
  }

  @override
  void mainPhase() async {
    updater = Timer.periodic(const Duration(seconds: 1), (_) {
      if (!isMainPhase) return;
      _playNext();
    });
  }

  // Play next sound in sequence
  void _playNext() {
    if (++currentlyPlayingIndex >= sequence.length) currentlyPlayingIndex = 0;
    audioCache.play(sequence[currentlyPlayingIndex]);
  }

  @override
  bool validateInput(_) =>
      currentlyPlayingIndex >= 0 &&
      sequence[currentlyPlayingIndex] == correctAnimal.fileName;
}
