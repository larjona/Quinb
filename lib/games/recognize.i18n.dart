// This is a generated file; do not edit
import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {
  static final _t = Translations.byLocale('en_us') +
      {
        'ru': {
          'Tap when you hear the previous sequence':
              'Нажми когда ты услышишь предыдушее звучание',
          'Tap here to listen to the sample':
              'Нажми сюда для прослушивания примера',
          'Tap here to start': 'Нажми здесь для старта',
          'Tap when you hear the previous sound':
              'Нажми когда ты услышишь предыдуший звук',
        },
        'it': {
          'Tap here to listen to the sample':
              'Tocca quando senti il suono di prima',
        }
      };

  String get i18n => localize(this, _t);
}
