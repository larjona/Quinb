# Italian translation for Quinb 1.0.0
# Copyright (C) 2020 DeepDaikon
# This file is distributed under the same license as Quinb.

#
msgid ""
msgstr ""
#: lib/ui/screens/settings/settings_page.dart:
msgid "SETTINGS"
msgstr "IMPOSTAZIONI"

#: lib/ui/screens/settings/settings_page.dart:
msgid "Restore"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Pause between rounds"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Wait before starting the next round"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Difficulty"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Difficulty of the games"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Points required"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Points required to win"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Round duration"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Time available each round (in seconds)"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Rounds"
msgstr ""

#: lib/ui/screens/settings/settings_page.dart:
msgid "Maximum number of rounds"
msgstr ""

