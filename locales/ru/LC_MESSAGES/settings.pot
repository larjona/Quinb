# Russian translation for Quinb 1.0.0
# Copyright (C) 2020 DeepDaikon
# This file is distributed under the same license as Quinb.

#
msgid ""
msgstr ""
#: lib/ui/screens/settings/settings_page.dart:
msgid "SETTINGS"
msgstr "НАСТРОЙКИ"

#: lib/ui/screens/settings/settings_page.dart:
msgid "Restore"
msgstr "Восстановить"

#: lib/ui/screens/settings/settings_page.dart:
msgid "Pause between rounds"
msgstr "Пауза между раундами"

#: lib/ui/screens/settings/settings_page.dart:
msgid "Wait before starting the next round"
msgstr "Подождите пока начнётся следующий раунд"

#: lib/ui/screens/settings/settings_page.dart:
msgid "Difficulty"
msgstr "Сложность"

#: lib/ui/screens/settings/settings_page.dart:
msgid "Difficulty of the games"
msgstr "Сложность игр"

#: lib/ui/screens/settings/settings_page.dart:
msgid "Points required"
msgstr "Очков требуется"

#: lib/ui/screens/settings/settings_page.dart:
msgid "Points required to win"
msgstr "Очков требуется для победы"

#: lib/ui/screens/settings/settings_page.dart:
msgid "Round duration"
msgstr "Продолжительность раунда"

#: lib/ui/screens/settings/settings_page.dart:
msgid "Time available each round (in seconds)"
msgstr "Время доступное на раунд (в секундах)"

#: lib/ui/screens/settings/settings_page.dart:
msgid "Rounds"
msgstr "Раунды"

#: lib/ui/screens/settings/settings_page.dart:
msgid "Maximum number of rounds"
msgstr "Максимальное число раундов"

