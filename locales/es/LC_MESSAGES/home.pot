# Spanish translation for Quinb 1.0.0
# Copyright (C) 2020 DeepDaikon
# This file is distributed under the same license as Quinb.

#
msgid ""
msgstr ""
#: lib/ui/screens/home/home_page.dart:
msgid "Play"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "Select game"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "Rules"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "Settings"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "Sound games"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "Sound-based games enabled"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "Sound-based games disabled"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "players"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "player"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "players"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "player"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "Enable volume?"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "\nIt looks like you have audio muted.\n\nYou should enable it in order to play sound-based games.\n"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "Remember choice"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "Cancel"
msgstr ""

#: lib/ui/screens/home/home_page.dart:
msgid "Enable"
msgstr ""

