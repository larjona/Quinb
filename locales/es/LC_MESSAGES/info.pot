# Spanish translation for Quinb 1.0.0
# Copyright (C) 2020 DeepDaikon
# This file is distributed under the same license as Quinb.

#
msgid ""
msgstr ""
#: lib/ui/screens/info/info_page.dart:
msgid "By"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "App developed by"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Version:"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "App version"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Report bugs"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Report bugs or request new feature"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "View source code"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Look at the source code"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Changelog"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "View app changelog"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "View License"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Read software license"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Third Party Licenses"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Read third party notices"
msgstr ""

#: lib/ui/screens/info/info_page.dart:
msgid "Third Party Licenses"
msgstr ""

